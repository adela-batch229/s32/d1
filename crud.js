let http = require("http");

// Mock or Dummy Data

let directory = [
	{
		"name" : "Brandon",
		"email" : "brandon@mail.com"
	},
	{
		"name" : "Jobert",
		"email" : "jobert@mail.com"
	}
]

http.createServer(function(req, res){
	// Route for returning all items upon receiving GET Method
	if(req.url == "/users" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "application/json"})
		// input has to be data type string for the application to read the incoming data properly.
		// stringify converts our JSON into string-like object.
		res.write(JSON.stringify(directory));
		// Ends the response process
		res.end();
	}

	if(req.url == "/users" && req.method == "POST"){
		// declare and initialize reqBody variable to an empty string
		// this will act as a placeholder for the data to be creater later.

		let reqBody = "";

		// this is where data insertion happens to our mock/dummy data
		req.on("data", function(data){
			// assigns the data retrieved from the stream to reqBody
			reqBody += data;
		});

		// response end step - only  runs after the request completely been set
		req.on("end", function(){
			console.log(typeof reqBody);

			// converts the strings to JSON
			reqBody = JSON.parse(reqBody);

			// create a new object representing the new mock data record
			let newUser = {
				"name" : reqBody.name,
				"email" : reqBody.email
			}
			directory.push(newUser);
			console.log(directory);

			res.writeHead(200, {"Content-Type" : "application/json"});
			res.write(JSON.stringify(newUser));
			res.end();
		});
	}
}).listen(3000);

console.log("CRUD is now running at port 3000");